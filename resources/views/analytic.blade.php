@extends('layouts.layout')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/home.css') }}">
@endsection

@section('title')
    123
@endsection

@section('body')
<div class="container"><div id="user-table"></div><div id="clicks-table"></div></div>

<script>

$(document).ready(function() {
    var url = 'https://cubingbattle.ru/api/v1/widgets/users';
    $.get(url, function(response) {
        console.log(response);
        var table = new Tabulator('#user-table', {
            data: JSON.parse(JSON.stringify(response)),
            layout: 'fitColumns',
            columns: [
                { title: 'Id', field: 'id'},
                { title: 'Name', field: 'name'},
                { title: 'Email', field: 'email' },
                { title: 'Brochures Clicks', field: 'Brochures' },
                { title: 'Attend Webinar Clicks', field: 'Attend_Webinar' },
                { title: 'Agenda Clicks', field: 'Agenda' }
            ]
        });
    });

    var url = 'https://cubingbattle.ru/api/v1/widgets/clicks';
    $.get(url, function(response) {
        console.log(response);
        var table = new Tabulator('#clicks-table', {
            data: JSON.parse(JSON.stringify(response)),
            layout: 'fitColumns',
            columns: [
                { title: 'Button', field: 'button'},
                { title: 'Clicks', field: 'count'}
            ]
        });
    });
});

</script>
@endsection
