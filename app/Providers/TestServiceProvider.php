<?php 

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TestServiceProvider extends ServiceProvider {
	
	public function register() {
		$this->app->bind('TestService', 'App\Services\TestService');
	}

}